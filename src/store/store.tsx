import { createStore, applyMiddleware } from "redux";
// import thunk from 'redux-thunk';
import createSagaMiddleware from "redux-saga";

import { createLogger } from "redux-logger";
import { composeWithDevTools } from "redux-devtools-extension";
import reducer from "../Reducers/rootReducer";
import mySaga from "../models/authReducer/sagas";

const sagaMiddleware = createSagaMiddleware();
const logger = (createLogger as any)();

let middleware = applyMiddleware(logger, sagaMiddleware);

if (process.env.NODE_ENV === "development") {
  middleware = composeWithDevTools(middleware);
}

export const store = createStore(reducer, {}, middleware);

sagaMiddleware.run(mySaga);
