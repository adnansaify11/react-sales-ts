import { combineReducers } from "redux";

import authReducer, { AuthState } from "../models/authReducer/reducer";

export interface AppState {
  auth: AuthState;
}

// const initialAppState: AppState = {
//   hello: helloInitialState
// };

export const rootReducer = combineReducers<AppState>({
  auth: authReducer
});

export default rootReducer;
