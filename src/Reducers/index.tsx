import { combineReducers } from "redux"
import * as test from "./testReducer"


export interface ReduxState {
    // counterReducer: test.State
    counterReducer: {}

  }

export const reducer =  combineReducers<ReduxState>({
	counterReducer: test.reducer
})  