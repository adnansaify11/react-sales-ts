import * as React from "react";
import AppDrawer from "./Common/AppDrawer";

interface Iprops {
  history: any;
}
class App extends React.Component<Iprops> {
  public render() {
    console.log(this.props, "props");
    return (
      <div>
        <AppDrawer history={this.props.history} />
      </div>
    );
  }
}

export default App;
