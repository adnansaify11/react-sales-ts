import * as React from "react";
import classNames from "classnames";
import { Link } from "react-router-dom";
import { bindActionCreators, Dispatch } from "redux";
import { connect } from "react-redux";
import Divider from "@material-ui/core/Divider";
import {
  AppBar,
  Toolbar,
  Drawer,
  WithStyles,
  Theme,
  createStyles,
  withStyles,
  IconButton,
  Hidden,
  ListItem,
  List,
  ListItemText,
  Collapse
} from "@material-ui/core";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import MenuIcon from "@material-ui/icons/Menu";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import Typography from "@material-ui/core/Typography";

import { LoginRequest, Action } from "../../models/authReducer/actions";

import MyPackage1 from "../Packages/package#1";
import MyPackage2 from "../Packages/package#2";
import ErrorPackage from "../Packages/ErrorPackage";
import items from "../../../src/Menuitems.json";

const drawerWidth = 185;
const styles = (theme: Theme) =>
  createStyles({
    root: {
      display: "flex"
    },

    drawer: {
      [theme.breakpoints.up("lg")]: {
        width: drawerWidth,
        flexShrink: 0
      }
    },

    appBar: {
      marginLeft: drawerWidth,
      backgroundColor: "#ff6666",
      [theme.breakpoints.up("lg")]: {
        width: `calc(100% - ${drawerWidth}px)`
      }
    },

    menuButton: {
      marginRight: 20,
      [theme.breakpoints.up("lg")]: {
        display: "none"
      }
    },

    onOpenMenuButton: {
      display: "none"
    },

    toolbar: {
      display: "flex",
      alignItems: "center",
      padding: "0 8px",
      ...theme.mixins.toolbar,
      justifyContent: "center",
      cursor: `pointer`
    },

    drawerPaper: {
      width: drawerWidth,
      backgroundColor: "#F4F5F7",
      "&::-webkit-scrollbar": {
        backgroundColor: "#F4F5F7",
        width: 4
      },
      "&::-webkit-scrollbar-thumb": {
        backgroundColor: "#ff6666",
        width: 6
      }
    },

    content: {
      flexGrow: 1,
      padding: theme.spacing.unit * 3
    },

    onOpenContent: {
      flexGrow: 1,
      padding: theme.spacing.unit * 3,
      marginLeft: `${drawerWidth - 15}px`
    },

    nested: {
      paddingLeft: theme.spacing.unit * 0
    },

    listItemText: {
      paddingLeft: "20px !important"
    },

    listItem: {
      "&:hover": {
        backgroundColor: "#ff6666"
      }
    },

    homeButton: {
      textDecoration: "none",
      cursor: "pointer",
      color: "white"
    },

    onOpenHomeButton: {
      textDecoration: "none",
      cursor: "pointer",
      color: "white",
      marginLeft: `${drawerWidth - 10}px`,
      marginRight: 0
    },

    drawerHeader: {
      ...theme.mixins.toolbar,
      display: "flex",
      alignItems: "center",
      padding: "0 8px",
      justifyContent: "flex-end",
      [theme.breakpoints.up("lg")]: {
        display: "none"
      }
    },

    icon: {
      color: `rgb(255, 255, 255)`
    },

    iconWrapper: {
      backgroundColor: `#ff6666`,
      width: 40,
      height: 40,
      display: `flex`,
      justifyContent: `center`,
      alignItems: `center`,
      borderRadius: 3,
      marginRight: 15
    }
  });

export interface DispatchProps {
  onLogin: (value: string) => Action;
}
interface Iprops extends WithStyles<typeof styles> {
  theme: Theme;
  history: any;
}

interface Istate {
  mobileOpen: boolean;
  selectedPackage: string;
  openItem: string;
  toggledropdown: boolean;
}

type Itest = Iprops & DispatchProps;

// type Iprops1 = DispatchProps & Iprops;
export class AppDrawer extends React.Component<Itest, Istate> {
  constructor(props: any) {
    super(props);
    this.state = {
      mobileOpen: false,
      openItem: "null",
      selectedPackage: "MyPackage1",
      toggledropdown: false
    };
  }

  public handleclick = (ParentItem: string, SubItems: boolean) => {
    if (SubItems === false) {
      this.setState({ selectedPackage: `${ParentItem}`, mobileOpen: false });
    } else {
      this.setState({ selectedPackage: `${ParentItem}` });
    }
    if (ParentItem !== "Overview" && ParentItem !== "Packages") {
      this.props.history.push(`/${ParentItem}/Overview`);
    } else {
      this.props.history.push(`/${ParentItem}/`);
    }

    if (this.state.openItem !== ParentItem) {
      this.setState({ openItem: `${ParentItem}`, toggledropdown: true });
    } else {
      this.setState({
        openItem: `${ParentItem}`,
        toggledropdown: !this.state.toggledropdown
      });
    }
  };

  public renderComponent = (PackageName: string, SubPackageName: string) => {
    console.log(this.props);
    this.props.onLogin("5");
    this.props.history.push(`/${PackageName}/${SubPackageName}`);
    this.setState({ selectedPackage: `${SubPackageName}`, mobileOpen: false });
  };

  public render() {
    const myComponents = {
      MyPackage1,
      MyPackage2,
      sub11: MyPackage1,
      sub12: MyPackage2,
      sub21: MyPackage1,
      sub22: MyPackage2,
      sub23: MyPackage1,
      sub24: MyPackage2,
      sub25: MyPackage1,
      sub26: MyPackage2,
      sub27: MyPackage1,
      sub28: MyPackage2
    };

    const { classes, theme } = this.props;
    const state = this.state;
    const Package = myComponents[this.state.selectedPackage] || ErrorPackage;

    const drawer = (
      <div>
        <div
          className={classNames(classes.toolbar, {
            [classes.drawerHeader]: state.mobileOpen === true
          })}
          onClick={(e: React.MouseEvent<HTMLElement>) =>
            this.handleclick(`Packages`, false)
          }
        >
          <div className={classes.iconWrapper}>
            <span className={classes.icon}>
              <svg
                width="24"
                height="24"
                viewBox="0 0 24 24"
                focusable="false"
                role="presentation"
              >
                <g fill="currentColor" fill-rule="evenodd">
                  <path d="M5 17.991c0 .007 14.005.009 14.005.009-.006 0-.005-7.991-.005-7.991C19 10.002 4.995 10 4.995 10 5.001 10 5 17.991 5 17.991zM3 10.01C3 8.899 3.893 8 4.995 8h14.01C20.107 8 21 8.902 21 10.009v7.982c0 1.11-.893 2.009-1.995 2.009H4.995A2.004 2.004 0 0 1 3 17.991V10.01z" />
                  <path d="M7 8.335c0-.002 2.002-.002 2.002-.002C9 8.333 9 6.665 9 6.665c0 .002-2.002.002-2.002.002C7 6.667 7 8.335 7 8.335zm-2-1.67C5 5.745 5.898 5 6.998 5h2.004C10.106 5 11 5.749 11 6.665v1.67C11 9.255 10.102 10 9.002 10H6.998C5.894 10 5 9.251 5 8.335v-1.67zm10 1.67c0-.002 2.002-.002 2.002-.002C17 8.333 17 6.665 17 6.665c0 .002-2.002.002-2.002.002.002 0 .002 1.668.002 1.668zm-2-1.67C13 5.745 13.898 5 14.998 5h2.004C18.106 5 19 5.749 19 6.665v1.67c0 .92-.898 1.665-1.998 1.665h-2.004C13.894 10 13 9.251 13 8.335v-1.67z" />
                </g>
              </svg>
            </span>
          </div>
          <Typography
            variant="subtitle1"
            color="inherit"
            noWrap
            align="justify"
          >
            Packages
          </Typography>
          <div>
            <IconButton
              className={classes.drawerHeader}
              onClick={() => this.setState({ mobileOpen: false })}
            >
              <ChevronLeftIcon />
            </IconButton>
          </div>
        </div>
        <Divider />
        {items.map((item: any) => (
          <div key={item.id}>
            {item.subitems.length !== 0 ? (
              <div key={item.id}>
                <ListItem
                  className={classes.listItem}
                  button
                  key={`${item.id}`}
                  onClick={(e: React.MouseEvent<HTMLElement>) =>
                    this.handleclick(`${item.title}`, true)
                  }
                >
                  <ListItemText primary={`${item.title}`} />
                  {state.openItem === `${item.title}` &&
                  state.toggledropdown === true ? (
                    <ExpandLess />
                  ) : (
                    <ExpandMore />
                  )}
                </ListItem>
                <Collapse
                  key={item.subitems.name}
                  in={
                    state.openItem === `${item.title}` &&
                    state.toggledropdown === true
                      ? true
                      : false
                  }
                  timeout="auto"
                  unmountOnExit
                  className={classes.nested}
                >
                  <List disablePadding>
                    {item.subitems.map((subitems: any, index: number) => (
                      <div key={index}>
                        <ListItem
                          className={classes.listItem}
                          button
                          onClick={(e: React.MouseEvent<HTMLElement>) =>
                            this.renderComponent(
                              `${item.title}`,
                              `${subitems.name}`
                            )
                          }
                        >
                          <ListItemText
                            className={classes.listItemText}
                            primary={`${subitems.name}`}
                          />
                        </ListItem>
                      </div>
                    ))}
                  </List>
                </Collapse>
              </div>
            ) : (
              <div key={item.id}>
                <ListItem
                  className={classes.listItem}
                  button
                  key={`${item.id}`}
                  onClick={(e: React.MouseEvent<HTMLElement>) =>
                    this.handleclick(`${item.title}`, false)
                  }
                >
                  <ListItemText primary={`${item.title}`} />
                </ListItem>
              </div>
            )}
          </div>
        ))}
        <Divider />
      </div>
    );

    return (
      <div className={classes.root}>
        <AppBar position="fixed" className={classes.appBar}>
          <Toolbar>
            <IconButton
              color="inherit"
              onClick={() => this.setState({ mobileOpen: true })}
              className={classNames(classes.menuButton, {
                [classes.onOpenMenuButton]: this.state.mobileOpen === true
              })}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" color="inherit" noWrap={true}>
              <Link
                className={classNames(classes.homeButton, {
                  [classes.onOpenHomeButton]: this.state.mobileOpen === true
                })}
                to="/"
              >
                React sales
              </Link>
            </Typography>
          </Toolbar>
        </AppBar>

        <nav className={classes.drawer}>
          <Hidden lgUp implementation="css">
            <Drawer
              classes={{
                paper: classes.drawerPaper
              }}
              open={this.state.mobileOpen}
              anchor={theme.direction === "rtl" ? "right" : "left"}
              variant="temporary"
              onClose={() => this.setState({ mobileOpen: false })}
            >
              {drawer}
            </Drawer>
          </Hidden>
          <Hidden mdDown implementation="css">
            <Drawer
              classes={{
                paper: classes.drawerPaper
              }}
              variant="permanent"
              open={true}
            >
              {drawer}
            </Drawer>
          </Hidden>
        </nav>

        <main
          className={classNames(classes.content, {
            [classes.onOpenContent]: this.state.mobileOpen === true
          })}
        >
          <div className={classes.toolbar} />
          <Package />
        </main>
      </div>
    );
  }
}

function mapStateToProps() {
  console.log("test state");
}

const mapDispatchToProps = (dispatch: Dispatch<Action>): DispatchProps => ({
  onLogin: bindActionCreators(LoginRequest, dispatch)
});

export default withStyles(styles, { withTheme: true })(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(AppDrawer)
);
