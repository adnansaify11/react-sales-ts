export interface LoginRequest {
  type: "LOGIN_REQUEST";
  payload: string;
}

export const LoginRequest = (value: string): LoginRequest => ({
  type: "LOGIN_REQUEST",
  payload: value
});

export type Action = LoginRequest;
