import { call, delay, put, takeEvery, takeLatest } from "redux-saga/effects";
import { Action } from "./actions";

function* fetchUser(action: Action) {
  console.log("test", action.payload);
  yield delay(5);

  console.log("testing saga");
}

export default function* mySaga() {
  console.log("MYSAGA");
  yield takeEvery("LOGIN_REQUEST", fetchUser);
}
