import { Reducer } from "redux";

import { Action as AuthAction } from "./actions";

export interface AuthState {
  value: string;
}

export const initialState: AuthState = {
  value: "0"
};

export const reducer: Reducer<AuthState> = (
  state: AuthState = initialState,
  action: AuthAction
) => {
  switch (action.type) {
    case "LOGIN_REQUEST":
      return {
        ...state,
        value: action.payload
      };
    default:
      return state;
  }
};

export default reducer;
