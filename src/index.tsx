import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import registerServiceWorker from './registerServiceWorker';
import { BrowserRouter as Router, Route,Switch} from 'react-router-dom'

import App from './Components/App';
import AppDrawer from './Components/Common/AppDrawer'
import {store} from './store/store'

ReactDOM.render(
  <Provider store={store}>
  <Router> 
    <Switch>
      <Route exact path="/" component={App} />
      <Route exact path="/:package/:subpackage?" component={AppDrawer} />
    </Switch>
  </Router>
  </Provider>,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
